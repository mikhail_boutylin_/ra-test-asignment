function CookiesHelper() { }
CookiesHelper.prototype.getCookie = function getCookie(cname) {
  var name = cname + '=';
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1);
    if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
  }
  return "";
};
CookiesHelper.prototype.setCookie = function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = 'expires=' + d.toUTCString();
  document.cookie = cname + '=' + cvalue + '; ' + expires;
};

CookiesHelper.prototype.generateUid = function generateUid() {
  //not really unique, but ok for our test :)
  var time = new Date().getTime();
  var randomPart = Math.floor(Math.random() * 10000);
  return time.toString() + randomPart.toString();
};


var cookiesHelper = new CookiesHelper();