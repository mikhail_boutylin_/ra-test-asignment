function ParametersHelper() {
}
ParametersHelper.prototype.getPageParameters = function () {
  if (!this._params) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    var len = vars.length;
    var arr = {};
    for (var i = 0; i < len; i++) {
      var param = vars[i].substring(0, vars[i].indexOf('='));
      arr[param] = decodeURIComponent(vars[i].substring(vars[i].indexOf('=') + 1));
    }
    this._params = arr;
  }
  return this._params;
};

ParametersHelper.prototype.getPageParam = function (key) {
  return this.getPageParameters()[key];
};

var parametersHelper = new ParametersHelper();