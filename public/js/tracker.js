function init() {
  function checkCookie() {
    if(!cookiesHelper.getCookie(CookieNames.USER)) {
      cookiesHelper.setCookie(CookieNames.USER, cookiesHelper.generateUid());
      //if browser prevented cookie set
      if(!cookiesHelper.getCookie(CookieNames.USER)) {
        window.top.location = 'http://test-analytics-tracker.com/cookie-creator.html?go_back_to=' + parametersHelper.getPageParam('current_page');
        return;
      }
    }
    var paragraph = document.createElement('p');
    paragraph.textContent = cookiesHelper.getCookie(CookieNames.USER);
    document.body.appendChild(paragraph);
  }
  checkCookie();
}