To run this locally, perform following commands in the directory you've cloned it to:
```
npm install
node app.js
```

And also to make it running on different domains assuming you're on MacOs add

in **/etc/hosts**
```
127.0.0.1 test-belaruss-closes.com test-unknown-closes.com test-analytics-tracker.com
```
In a new file **/etc/apache2/other/crossdomaincookies.conf**
```
<VirtualHost *:80>
  ProxyPreserveHost On
  ProxyRequests Off
  ServerAlias test-belaruss-closes.com
  ServerName test-belaruss-closes.com
  ProxyPass / http://localhost:35000/
  ProxyPassReverse / http://localhost:35000/
</VirtualHost>

<VirtualHost *:80>
  ServerName test-unknown-closes.com
  ServerAlias test-unknown-closes.com
  ProxyPass / http://localhost:35001/
  ProxyPassReverse / http://localhost:35001/
</VirtualHost>

<VirtualHost *:80>
  ServerName test-analytics-tracker.com
  ServerAlias test-analytics-tracker.com
  ProxyPass / http://localhost:35001/
  ProxyPassReverse / http://localhost:35001/
</VirtualHost>
```

You can check it within following links: 
http://test-belaruss-closes.com 
http://test-unknown-closes.com