'use strict';
var express = require('express');
var path = require('path');

function createServer(port) {
  var app = express();
  app.use(express.static(path.join(__dirname, 'public')));
  app.listen(port);
  return app;
}

createServer(35000); //test-belaruss-closes.com
createServer(35001); //test-unknown-closes.com
createServer(35002); //test-analytics-tracker.com
